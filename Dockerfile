FROM nginx:stable
LABEL maintainer="Greg Farr"

RUN apt-get update && apt-get -y install \
    certbot python3-certbot-nginx
